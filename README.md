### oracle-instantclient

This role installs sqlplus for debian based linux distribution.   
To use this role, you'll need to overload oracle_instantclient_archives like the following example:
```
- role: oracle-instantclient
      oracle_instantclient_version: 18.5
      oracle_instantclient_base_dir: /opt/oracle
      oracle_instantclient_archives:
        - http://server_name/instantclient-basiclite-linux.x64-18.5.0.0.0dbru.zip
        - http://server_name/instantclient-sqlplus-linux.x64-18.5.0.0.0dbru.zip
        - http://server_name/instantclient-tools-linux.x64-18.5.0.0.0dbru.zip
```

### Variables

| variables name                | optionnal | default value | description|
|-------------------------------|-----------|---------------|----------------------------------------|
| oracle_instantclient_version  | yes       | 18.5          | version                                |
| oracle_instantclient_base_dir | yes       | /opt/oracle   | target path                            |
| oracle_instantclient_archives | no        |               | lists for binaries (should be url)       |


### Sample playbook

You could see a sample playbook in [tests](tests/test_oracle-instantclient_stretch)
