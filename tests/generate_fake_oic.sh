#!/bin/bash

version=${1-18.3}
dirname=instantclient_${version/\./_}

mkdir -p ${dirname}
printf "#!/bin/bash\n\n printf \"Version ${version}.0.0.0\n\"" > ${dirname}/sqlplus
chmod a+x ${dirname}/sqlplus

zip --quiet -9 --recurse-paths instantclient-sqlplus-linux.x64-${version}.0.0.0dbru.zip ${dirname}
rm -rf ${dirname}

